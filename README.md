# Node pnpm Docker

Node pnpm is a Docker container configuration useful to develop web UI can be easily integrated in any project.

## Installation

Use Make command `make docker` it creates a **node-pnpm** image ready to be used in docker-compose

```bash
make docker
```

## Usage

you need to mount your project root directory to container /home/node/app 

```yaml
# docker-composer-vite.yaml
services:
  vite:
    image: node-pnpm:16-alpine
    entrypoint: /bin/sh
    command: -c "pnpm i && pnpm dev"
    ports:
      - "3000:3000"
    volumes:
      - .:/home/node/app
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)